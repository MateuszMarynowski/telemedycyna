import tkinter
import cv2
import PIL.Image, PIL.ImageTk
import time
import dlib
import imutils
from imutils import face_utils
import numpy as np
from scipy.spatial import distance
import sys

path = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(path)
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

dist_min = 1
dist_max = 1
X = 0
Y = 0
W = 0
H = 0
capture_start = False

WINDOW_WIDTH = 800
WINDOW_HEIGHT = 700
COLOR_GRAY = "#5c5959"
COLOR_SEABLUE = "#547980"
COLOR_GREENBLUE = "#45AdA8"
COLOR_MINT = "#9de0ad"
COLOR_LIGHTGREEN ="#e5fcc2"

class App:
    def __init__(self, window, window_title, video_source=0):

        self.window = window
        self.window.title(window_title)
        self.video_source = video_source
        window.configure(bg=COLOR_GRAY)
        window.geometry("1000x530")
        self.vid = MyVideoCapture(self.video_source)
        self.canvas = tkinter.Canvas(window, width = self.vid.width, height = self.vid.height,  highlightthickness=0)
        self.canvas.place(x=25, y=25)

        load = PIL.Image.open("smile.jpg")
        render = PIL.ImageTk.PhotoImage(load)
        img = tkinter.Label(self.window, image=render)
        img.image = render
        img.place(x=720, y=25)

        # Start button
        self.btn_snapshot = tkinter.Button(window, text="Start", command=self.start, bg=COLOR_SEABLUE, fg="white")
        self.btn_snapshot.place(x=720, y=320, width=210, height=35)

        # Button that lets the user do calibration
        self.btn_snapshot = tkinter.Button(window, text="Calibration", command=self.calibration, bg=COLOR_SEABLUE, fg="white")
        self.btn_snapshot.place(x=720, y=370, width=210, height=35)

        # Button that lets the user take a snapshot
        self.btn_snapshot = tkinter.Button(window, text="Snapshot", command=self.snapshot, bg=COLOR_SEABLUE, fg="white")
        self.btn_snapshot.place(x=720, y=420, width=210, height=35)

        # Quit button
        self.btn_snapshot = tkinter.Button(window, text="Quit", command=self.quit, bg=COLOR_SEABLUE, fg="white")
        self.btn_snapshot.place(x=720, y=470, width=210, height=35)

        # After it is called once, the update method will be automatically called every delay milliseconds
        self.delay = 5
        self.update()

        self.window.mainloop()

    def countdown(self, remaining=None):
        if remaining is not None:
            self.remaining = remaining
        if self.remaining <= 0:
            self.label.configure(text="Kalibracja w toku")
        else:
            self.label.configure(text="%d" % self.remaining)
            self.remaining = self.remaining - 1
            self.window.after(1000, self.countdown)

    def snapshot(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if ret:
           cv2.imwrite("frame-" + time.strftime("%d-%m-%Y-%H-%M-%S") + ".jpg", cv2.cvtColor(frame, cv2.COLOR_RGB2BGR))

    def update(self):
        global capture_start
        # Get a frame from the video source
        if capture_start:
            ret, frame = self.vid.get_frame()
            if ret:
                rects = detector(frame, 1)
                for (i, rect) in enumerate(rects):
                        # determine the facial landmarks for the face region, then
                        # convert the landmark (x, y)-coordinates to a NumPy array
                    shape = predictor(frame, rect)
                    shape = face_utils.shape_to_np(shape)
                    jaw = shape[8]
                    nose = shape[30]
                    dist = distance.euclidean(nose, jaw)
                    ratio = (dist - dist_min) / (dist_max - dist_min)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    if ratio < 0:
                        ratio = 0
                    elif ratio > 1 and ratio < 1.2:
                        ratio = 1
                    elif ratio > 1.2:
                        cv2.putText(frame, "ALERT", (10, 60), font, 1,
                                    (0, 255, 0), 2, cv2.LINE_AA)
                        ratio = float('Inf')
                    cv2.putText(frame, "current ratio : " + str(round(ratio, 2)), (10, 30), font, 1,
                                    (0, 255, 0), 2, cv2.LINE_AA)
                    (x, y) = shape[8]
                    cv2.circle(frame, (x, y), 5, (0, 0, 255), -1)
                    (x, y) = shape[30]
                    cv2.circle(frame, (x, y), 5, (0, 0, 255), -1)
                    cv2.rectangle(frame, (X, Y), (X + W, Y + H), (0, 255, 0), 2)
                        # cv2.rectangle(frame, (210, 100), (430, 420), (255, 0, 0), 2)
                    self.photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(frame))
                    self.canvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)
        self.window.after(self.delay, self.update)

    def start(self):
        global capture_start
        capture_start = True

    def quit(self):
        sys.exit(0)

    def timer(self):
        self.label = tkinter.Label(self.window, text="", width=10)
        self.label.pack()
        self.remaining = 0
        self.countdown(3)

    def forget(self):
        self.label.pack_forget()

    def calibration(self):
        self.timer()
        self.window.after(4000, self.calibrate_min)
        self.window.after(4000, self.forget)
        self.window.after(4000, self.timer)
        self.window.after(8000, self.calibrate_max)
        self.window.after(8000, self.forget)

    def calibrate_min(self):
        global dist_min
        global X, Y, W, H
        ret, frame = self.vid.get_frame()
        if ret:
            rects = detector(frame, 1)
            faces = faceCascade.detectMultiScale(
                frame,
                scaleFactor=1.1,
                minNeighbors=5,
                minSize=(50, 50),
                flags=cv2.CASCADE_SCALE_IMAGE
            )
            for (x, y, w, h) in faces:
                X = x
                Y = y
                W = w
                H = h
            for (i, rect) in enumerate(rects):
                shape = predictor(frame, rect)
                shape = face_utils.shape_to_np(shape)
                jaw = shape[8]
                nose = shape[30]
                dist_min = distance.euclidean(nose, jaw)
                (x, y) = shape[8]
                cv2.circle(frame, (x, y), 5, (0, 0, 255), -1)
                (x, y) = shape[30]
                cv2.circle(frame, (x, y), 5, (0, 0, 255), -1)
                self.photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(frame))
                self.canvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)

    def calibrate_max(self):
        global dist_max
        ret, frame = self.vid.get_frame()
        if ret:
            rects = detector(frame, 1)
            for (i, rect) in enumerate(rects):
                shape = predictor(frame, rect)
                shape = face_utils.shape_to_np(shape)
                jaw = shape[8]
                nose = shape[30]
                dist_max = distance.euclidean(nose, jaw)
                (x, y) = shape[8]
                cv2.circle(frame, (x, y), 5, (0, 0, 255), -1)
                (x, y) = shape[30]
                cv2.circle(frame, (x, y), 5, (0, 0, 255), -1)
                self.photo = PIL.ImageTk.PhotoImage(image=PIL.Image.fromarray(frame))
                self.canvas.create_image(0, 0, image=self.photo, anchor=tkinter.NW)

class MyVideoCapture:
    def __init__(self, video_source=0):
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

         # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                # Return a boolean success flag and the current frame converted to BGR
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (ret, None)

     # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

# Create a window and pass it to the Application object
App(tkinter.Tk(), "Pomiar otwarcia ust")